symfony-rover
=============

A Symfony project created on October 1, 2017, 2:12 pm.

# Installing
```
git clone git@gitlab.com:diegonobre/symfony-rover.git
cd symfony-rover
composer install
```

# Running
```
php bin/console rovers:move
```

# Testing
```
vendor/bin/phpunit
```
