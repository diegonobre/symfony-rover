<?php

namespace Tests\AppBundle\Command;

use AppBundle\Command\RoverSquadCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class RoverSquadCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        self::bootKernel();
        $application = new Application(self::$kernel);

        $application->add(new RoverSquadCommand());

        $command = $application->find('rovers:move');
        $commandTester = new CommandTester($command);

        $commandTester->execute(array(
            'command'  => $command->getName(),
        ));

        $output = $commandTester->getDisplay();
        $this->assertContains('1 3 N', $output);
        $this->assertContains('5 1 E', $output);
    }
}
