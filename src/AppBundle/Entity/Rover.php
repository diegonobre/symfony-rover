<?php

namespace AppBundle\Entity;

class Rover
{
    /**
     * @var string
     */
    private $direction;

    /**
     * @var integer
     */
    private $xPosition;

    /**
     * @var integer
     */
    private $yPosition;

    public function __construct($x, $y, $direction)
    {
        $this->xPosition = $x;
        $this->yPosition = $y;
        $this->direction = $direction;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * @return int
     */
    public function getXPosition()
    {
        return $this->xPosition;
    }

    /**
     * @param int $xPosition
     */
    public function setXPosition($xPosition)
    {
        $this->xPosition = $xPosition;
    }

    /**
     * @return int
     */
    public function getYPosition()
    {
        return $this->yPosition;
    }

    /**
     * @param int $yPosition
     */
    public function setYPosition($yPosition)
    {
        $this->yPosition = $yPosition;
    }

    public function move()
    {
        switch ($this->direction) {
            case 'N':
                $this->yPosition++;
                break;
            case 'E':
                $this->xPosition++;
                break;
            case 'S':
                $this->yPosition--;
                break;
            case 'W':
                $this->xPosition--;
                break;
        }
    }

    public function turnLeft()
    {
        $newDirection = '';
        switch ($this->direction) {
            case 'N':
                $newDirection = 'W';
                break;
            case 'E':
                $newDirection = 'N';
                break;
            case 'S':
                $newDirection = 'E';
                break;
            case 'W':
                $newDirection = 'S';
                break;
        }

        $this->direction = $newDirection;
    }

    public function turnRight()
    {
        $newDirection = '';
        switch ($this->direction) {
            case 'N':
                $newDirection = 'E';
                break;
            case 'E':
                $newDirection = 'S';
                break;
            case 'S':
                $newDirection = 'W';
                break;
            case 'W':
                $newDirection = 'N';
                break;
        }

        $this->direction = $newDirection;
    }

    public function loadInstruction($instruction)
    {
        switch ($instruction) {
            case 'M':
                $this->move();
                break;
            case 'L':
                $this->turnLeft();
                break;
            case 'R':
                $this->turnRight();
                break;
        }
    }
}
