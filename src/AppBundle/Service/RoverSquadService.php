<?php

namespace AppBundle\Service;

use AppBundle\Entity\Rover;

class RoverSquadService
{
    private $squadRoute = '5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM';

    /**
     * Returns entire squad rovers data
     *
     * @return array
     */
    public function getRoversRoute()
    {
        return explode('\n', $this->squadRoute);
    }

    /**
     * Set new squad route
     *
     * @param $route
     */
    public function setRoversRoute($route)
    {
        $this->squadRoute = $route;
    }

    /**
     * Process squad data and return array of rovers with actual positions
     *
     * @return array
     */
    public function processSquadRoute()
    {
        $rovers = array();

        $roversRoute = $this->getRoversRoute();
        unset($roversRoute[0]);
        $rover = new \stdClass();

        // read rovers data and move on
        foreach ($roversRoute as $key => $value) {
            // setting rover position and direction
            if ($key % 2 != 0) {
                $position = explode(' ', $value);
                $rover = new Rover($position[0], $position[1], $position[2]);
            }

            // moving rovers over plateau
            if ($key % 2 == 0) {
                $movements = str_split($value);

                foreach ($movements as $movement) {
                    $rover->loadInstruction($movement);
                }

                array_push($rovers, $rover);
            }
        }

        return $rovers;
    }
}
