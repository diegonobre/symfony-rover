<?php

namespace AppBundle\Command;

use AppBundle\Entity\Rover;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\RoverSquadService;

class RoverSquadCommand extends Command
{
    protected function configure()
    {
        $this
            // some basic configuration
            ->setName('rovers:move')
            ->setDescription('Read data and move squad over the plateau.')
            ->setHelp('Use this command to move the entire squad.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rovers = (new RoverSquadService())->processSquadRoute();

        foreach ($rovers as $rover) {
            $output->writeln($rover->getXPosition() . ' ' . $rover->getYPosition() . ' ' . $rover->getDirection());
        }
    }
}
